﻿using UnityEngine;
using System.Collections;

public class ShatterCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Wall")
		{
			Runner runner = transform.parent.GetComponent<Runner>();
			runner.Explode();
		}
	}

}
