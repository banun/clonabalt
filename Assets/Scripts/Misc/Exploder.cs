﻿using UnityEngine;
using System.Collections;

public class Exploder : MonoBehaviour {
	public float radius = 5.0F;
	public float power = 10.0F;
	// Use this for initialization
	void Start () {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders) {
			Rigidbody rb = hit.gameObject.GetComponent<Rigidbody>();
            
			if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
            
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
