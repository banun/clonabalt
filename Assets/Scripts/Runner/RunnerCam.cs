﻿using UnityEngine;
using System.Collections;

public class RunnerCam : MonoBehaviour {

	public Transform player = null;
	public float cameraHeight;
	private Transform cam = null;


	public void Start()
	{
		cam = transform;
	}
	public void Update()
	{
		Vector3 pos = player.position;
		pos.z = -10;
		pos.y += cameraHeight;
		cam.position = pos;
	}

}
