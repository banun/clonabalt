﻿using UnityEngine;
using System.Collections;

// TODO: update how runner checks if grounded
public class Runner : MonoBehaviour {

	public static float distanceTraveled;

  public float moveSpeed;
  public float groundedOffset;
  public Vector3 jumpVelocity;
	public Vector3 jumpRotation;

	public Rigidbody rb;
	public Transform remainsPrefab;
	public GameObject model;
	public GameObject overlay;

	private bool isAlive = true;
	private bool isGrounded = true;

	private Vector3 runDirection;

	// Use this for initialization
	void Start ()
  {
	}

	// Update is called once per frame
	void Update ()
	{
		if (isAlive)
		{
			transform.Translate(moveSpeed * Time.deltaTime, 0f, 0f);
			distanceTraveled = transform.localPosition.x;

			if (Input.GetKeyDown(KeyCode.Space))
			{
				rb.AddForce(jumpVelocity, ForceMode.VelocityChange);
			}

		}
	}

  void FixedUpdate()
  {
    isGrounded = IsGrounded();
  }

	public void Explode()
	{
		if (isAlive)
		{
			Instantiate (remainsPrefab, transform.position, transform.rotation);
			Destroy (model);
			moveSpeed = 0;
			overlay.SetActive (true);
		}

		isAlive = false;
	}

	public void SetGrounded ()
	{
		// slow down when hit the ground
		if (!isGrounded)
		{
			rb.AddForce (new Vector3 (-2, 0, 0), ForceMode.VelocityChange);
		}

  }

  // use raycasting to check if grounded
	private bool IsGrounded ()
	{
			bool groundHit;

      RaycastHit hit;

      groundHit = Physics.Raycast(transform.position, -transform.up, out hit, groundedOffset);

      return groundHit;
	}

}
