﻿using UnityEngine;
using System.Collections.Generic;

public class SkylineManager : MonoBehaviour {

	public Transform prefab;
	public int numberOfObjs;
	public float recycleOffset;
	public Vector3 startPosition;

	public Vector3 minSize, maxSize;

	private Vector3 nextPosition;
	private Queue<Transform> objectQueue;

    // Use this for initialization
    void Start()
    {
        nextPosition = startPosition;
		// create queue with numberOfObjs as length
		objectQueue = new Queue<Transform>(numberOfObjs);

		for (int i = 0; i < numberOfObjs; i++)
		{
			// create new block with prefab
			Transform block = Instantiate (prefab) as Transform;
			block.localPosition = nextPosition;
			// add width of block to get next po
			nextPosition.x += block.localScale.x;
			objectQueue.Enqueue (block);
		}

	}

    // Update is called once per frame
    void Update()
    {
		if (objectQueue.Peek().localPosition.x + recycleOffset < Runner.distanceTraveled)
		{
			Recycle ();
		}
	}

	private void Recycle()
	{
		Vector3 scale = new Vector3 (Mathf.Round(Random.Range (minSize.x, maxSize.x)), Mathf.Round(Random.Range (minSize.y, maxSize.y)), 1);

		Vector3 position = nextPosition;
		position.x += scale.x * 0.2f;
//		position.y += scale.y;

		Transform block = objectQueue.Dequeue ();
		block.localScale = scale;
		block.localPosition = position;
		nextPosition.x += scale.x;
		objectQueue.Enqueue (block);
	}

}
